from flask import Flask
from flask import render_template # Flask shortcut to render html & css pages
from flask import request
from flask import abort # Flask shortcut for errorhandling
import os # os import doesn't have to be in requirements.txt since it's incorprated to python

app = Flask(__name__)

@app.route("/<link>")
def hello(link):

    # Check for improper URL paths
    text = str(request.full_path)
    if(('//' in text) or ('~' in text) or ('..' in text)):
        abort(403)
    elif(link.endswith('.html') or link.endswith(".css")):
        # create path based on templates directory
        path = "templates/" + link
        # check if path exists using os library
        if os.path.exists(path):
            return render_template("200.html") + render_template(link)
        else:
            abort(404)
    else:
        abort(403)


@app.errorhandler(404) # special Flask errorhandler
def error_404(error):
    # returns html page from templates
    return render_template('404.html'), 404 #"404" to the left at the end displays the HTTP error as requested

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
